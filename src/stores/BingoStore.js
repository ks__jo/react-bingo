import { observable, action, decorate, reaction } from 'mobx';
import 'mobx-react-lite/batchingForReactDom';

const squareLength = 5;
const numbersSet = new Array(squareLength * squareLength).fill().map((item, index) => index + 1);
const buildBingoFromNumbersSet = () => numbersSet.sort(() => Math.random() - Math.random()).map((number) => { return {number, selected: false} });

export default class BingoStore {
	bingoNumbersList1;
	bingoNumbersList2;
	activePlayer;
	completedBingoList1;
	completedBingoList2;
	gameStarted;

	constructor() {
		this.initializeBingo();
	}

	initializeBingo = () => {
		this.bingoNumbersList1 = new Array(squareLength * squareLength);
		this.bingoNumbersList2 = new Array(squareLength * squareLength);
		this.activePlayer = 1;
		this.completedBingoList1 = [];
		this.completedBingoList2 = [];
		this.gameStarted = false;
	};

	shuffleBingo = () => {
		this.bingoNumbersList1 = buildBingoFromNumbersSet();
		this.bingoNumbersList2 = buildBingoFromNumbersSet();
		this.completedBingoList1 = [];
		this.completedBingoList2 = [];
		this.activePlayer = 1;
		this.gameStarted = true;
	};

	selectItem = (playerId, index) => {
		const isProperTurn = playerId === this.activePlayer;
		const clickedItem = this[`bingoNumbersList${playerId}`][index];
		const playerIdOfEnemy = playerId === 1 ? 2 : 1;
		if(!clickedItem.selected) {
			if (isProperTurn) {
				const selectTargetInEnemy = this[`bingoNumbersList${playerIdOfEnemy}`].findIndex((item) => item.number === clickedItem.number);
				this[`bingoNumbersList${playerId}`][index].selected = true;
				this[`bingoNumbersList${playerIdOfEnemy}`][selectTargetInEnemy].selected = true;
				this.activePlayer = playerIdOfEnemy;
				this.checkBingo(playerId, index);
				this.checkBingo(playerIdOfEnemy, selectTargetInEnemy);
			} else {
				alert('잘못된 차례입니다');
			}
		}
	};

	checkBingo = (playerId, index) => {
		let isBingo;

		//check diagonal from top left point
		if(index % (squareLength + 1) === 0) {
			isBingo = true;
			for(let i = 0; i < squareLength; i++) {
				if(!this[`bingoNumbersList${playerId}`][i * (squareLength + 1)].selected) {
					isBingo = false;
					break;
				}
			}
			if(isBingo) {
				this[`completedBingoList${playerId}`].push({
					type: 'topLeftDiagonal',
					index: 0
				});
			}
		}

		//check diagonal from top right point
		if(index > 0 && index % (squareLength - 1) === 0) {
			isBingo = true;
			for(let i = 1; i < squareLength + 1; i++) {
				if(!this[`bingoNumbersList${playerId}`][i * (squareLength - 1)].selected) {
					isBingo = false;
					break;
				}
			}
			if(isBingo) {
				this[`completedBingoList${playerId}`].push({
					type: 'topRightDiagonal',
					index: 0
				});
			}
		}

		//check row
		isBingo = true;
		const rowIndex = Math.floor(index / squareLength);
		for(let i = 0; i < squareLength; i++) {
			if(!this[`bingoNumbersList${playerId}`][rowIndex * squareLength + i].selected) {
				isBingo = false;
				break;
			}
		}
		if(isBingo) {
			this[`completedBingoList${playerId}`].push({
				type: 'row',
				index: rowIndex + 1
			});
		}

		//check column
		isBingo = true;
		const columnIndex = index % squareLength;
		for(let i = 0; i < squareLength; i++) {
			if(!this[`bingoNumbersList${playerId}`][i * squareLength + columnIndex].selected) {
				isBingo = false;
				break;
			}
		}
		if(isBingo) {
			this[`completedBingoList${playerId}`].push({
				type: 'column',
				index: columnIndex + 1
			});
		}
	};

	announceResult = reaction(
		() => this.completedBingoList1?.length >= 5 || this.completedBingoList2?.length >= 5,
		() => {
			if(this.completedBingoList1.length >= 5 && this.completedBingoList2.length >= 5) {
				alert('무승부입니다')
			} else if(this.completedBingoList1.length >= 5) {
				alert('1P가 빙고를 완성했습니다.')
			} else if(this.completedBingoList2.length >= 5) {
				alert('2P가 빙고를 완성했습니다.')
			}
			this.initializeBingo();
		}
	);
}



decorate(BingoStore, {
	bingoNumbersList1: observable,
	bingoNumbersList2: observable,
	initializeBingo: action,
	shuffleBingo: action,
	selectItem: action,
	checkBingo: action,
	completedBingoList1: observable,
	completedBingoList2: observable,
	gameStarted: observable,
	activePlayer: observable,
});
