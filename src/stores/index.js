import React from 'react';
import { MobXProviderContext } from 'mobx-react';
import BingoStore from "./BingoStore";

export const stores = {
	bingoStore: new BingoStore(),
};

export function useStores() {
	return React.useContext(MobXProviderContext);
}
