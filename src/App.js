import React from 'react';
import StartButton from './components/StartButton';
import BingoPlates from './components/BingoPlates';
import { AppContainer, AppOuterContainer } from './styles';

function App() {
  return (
    <AppOuterContainer>
      <AppContainer>
        <StartButton/>
        <BingoPlates/>
      </AppContainer>
    </AppOuterContainer>
  );
}

export default App;
