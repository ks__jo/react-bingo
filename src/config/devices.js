export default {
	DESKTOP: '992px',
	TABLET: '768px',
	PHONE: '415px',
};
