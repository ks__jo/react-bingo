import styled from "styled-components";

export const AppContainer = styled.div`
	max-width: 1200px;
	align-self: center;
	flex: 1;
`;

export const AppOuterContainer = styled.div`
	display: flex;
	justify-content: center;
`;
