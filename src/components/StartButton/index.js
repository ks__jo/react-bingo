import React from 'react';
import { useStores } from '../../stores';
import { observer } from 'mobx-react';
import { StartButtonContainer } from './styles';

function StartButton() {
	const { bingoStore } = useStores();
	return (
		<StartButtonContainer onClick={bingoStore.shuffleBingo}>
			{bingoStore.gameStarted ? '게임 재시작' : '게임 시작'}
		</StartButtonContainer>
	);
}

export default observer(StartButton);
