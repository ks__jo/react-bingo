import styled from "styled-components";

export const StartButtonContainer = styled.div`
	padding: 16px 0px;
	margin: 32px 32px 0px 32px;
	text-align: center;
	background-color: #134734;
	color: white;
`;
