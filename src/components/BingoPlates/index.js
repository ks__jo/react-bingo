import React from 'react';
import { useStores } from '../../stores';
import BingoPlate from './BingoPlate';
import CompletedBingoList from './CompletedBingoList';
import { BingoPlatesContainer, BingoPlateOuterContainer } from './styles';

function BingoPlates() {
	const { bingoStore } = useStores();
	return (
		<BingoPlatesContainer>
			<BingoPlateOuterContainer>
				<BingoPlate numbers={bingoStore.bingoNumbersList1} playerId={1}/>
				<CompletedBingoList playerId={1}/>
			</BingoPlateOuterContainer>
			<BingoPlateOuterContainer>
				<BingoPlate numbers={bingoStore.bingoNumbersList2} playerId={2}/>
				<CompletedBingoList playerId={2}/>
			</BingoPlateOuterContainer>
		</BingoPlatesContainer>
	);
}

export default BingoPlates;
