import React from 'react';
import { observer } from 'mobx-react';
import { useStores } from "../../stores";
import { BingoItemOuterContainer, BingoItemContainer } from './styles';

function BingoItem ({index, playerId}) {
	const { bingoStore } = useStores();
	const item = bingoStore[`bingoNumbersList${playerId}`][index];
	const onClickItem = () => {
		if(item) {
			bingoStore.selectItem(playerId, index);
		}
	};
	return (
		<BingoItemOuterContainer>
			<BingoItemContainer isSelected={item?.selected} onClick={onClickItem}>
				{item?.number}
			</BingoItemContainer>
		</BingoItemOuterContainer>
	);
}

export default observer(BingoItem);
