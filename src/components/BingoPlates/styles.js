import styled from "styled-components";
import devices from '../../config/devices';

export const BingoItemOuterContainer = styled.div`
	width: 100%;
	padding-top: 100%;
	position: relative;
`;
export const BingoItemContainer = styled.div`
	position: absolute;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	background-color: ${props => props.isSelected ? '#474747' : 'white'};
	color: ${props => props.isSelected ? 'white' : '#474747'};
	display: flex;
	justify-content: center;
	align-items: center;
	border: 1px solid #474747;
	font-size: 1.3rem;
`;

export const BingoPlateContainer = styled.div`
	display: grid;
	grid-template-columns: repeat(5, 1fr);
	margin: 16px;
	grid-gap: 3px;
	opacity: ${props => props.isActive ? 1 : 0.4};
`;

export const BingoPlateOuterContainer = styled.div`
	flex: 1;
`;

export const BingoPlatesContainer = styled.div`
	display: flex;
	padding: 16px;
	@media (max-width: ${devices.PHONE}) {
		flex-direction: column;
	}
`;

export const CompletedBingoListItemContainer = styled.div`
	display: flex;
	margin: 16px;
`;
