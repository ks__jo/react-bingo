import React from 'react';
import { useStores } from '../../stores';
import { observer } from 'mobx-react';
import CompletedBingoListItem from './CompletedBingoListItem';

function CompletedBingoList({playerId}) {
	const { bingoStore } = useStores();
	return (
		bingoStore[`completedBingoList${playerId}`].map((item, index) => {
			return (
				<CompletedBingoListItem item={item} index={index} key={index}/>
			);
		})
	);
}

export default observer(CompletedBingoList);
