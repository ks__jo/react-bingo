import React from 'react';
import { observer } from 'mobx-react';
import { CompletedBingoListItemContainer } from './styles';

const typeNames = {
	topLeftDiagonal: '우하향 대각선',
	topRightDiagonal: '좌하향 대각선',
	column: '열',
	row: '행',
};

function CompletedBingoListItem({item, index}) {
	return (
		<CompletedBingoListItemContainer>
			{`BINGO ${index + 1}: ` + (item.index > 0 ? item.index : '') + typeNames[item.type]}
		</CompletedBingoListItemContainer>
	);
}

export default observer(CompletedBingoListItem);
