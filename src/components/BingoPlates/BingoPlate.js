import React from 'react';
import BingoItem from './BingoItem';
import { useStores } from "../../stores";
import { BingoPlateContainer } from './styles';
import { observer } from 'mobx-react';

function BingoPlate ({playerId}) {
	const { bingoStore } = useStores();
	return (
		<BingoPlateContainer isActive={bingoStore.gameStarted && (bingoStore.activePlayer === playerId)}>
			{
				bingoStore[`bingoNumbersList${playerId}`].map((item, index) => {
					return (
						<BingoItem
							key={index}
							index={index}
							playerId={playerId}
						/>
					)
				})
			}
		</BingoPlateContainer>
	);
}

export default observer(BingoPlate);
